// Tooltips
$(document).ready(function() {
	// Tooltips
    	$("body").tooltip({ selector: '[data-toggle=tooltip]', html: true, placement: "top" });
});

$(document).ready(function() {
	// Table sorting
	$('.sortable').DataTable({
		"paging": false
	});
});