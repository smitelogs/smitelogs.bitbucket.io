const currentPatch = '9.2-9.3';

const urlParams = new URLSearchParams(window.location.search);

const gameModeIDsByName = {
	"arena": "435",
	"joust-ranked": "450",
	"slash": "10189",
};

const godIDsByName = {
	"kali": 1649,
	"anubis": 1668,
	"odin": 1669,
	"ymir": 1670,
	"zeus": 1672,
	"hun-batz": 1673,
	"he-bo": 1674,
	"hades": 1676,
	"kukulkan": 1677,
	"bastet": 1678,
	"ra": 1698,
	"arachne": 1699,
	"hel": 1718,
	"vamana": 1723,
	"agni": 1737,
	"sobek": 1747,
	"artemis": 1748,
	"bakasura": 1755,
	"guan-yu": 1763,
	"anhur": 1773,
	"cupid": 1778,
	"thor": 1779,
	"ares": 1782,
	"freya": 1784,
	"loki": 1797,
	"bacchus": 1809,
	"fenrir": 1843,
	"hercules": 1848,
	"xbalanque": 1864,
	"vulcan": 1869,
	"neith": 1872,
	"poseidon": 1881,
	"aphrodite": 1898,
	"apollo": 1899,
	"ne-zha": 1915,
	"eset": 1918,
	"athena": 1919,
	"chronos": 1920,
	"chang-e": 1921,
	"tyr": 1924,
	"zhong-kui": 1926,
	"mercury": 1941,
	"thanatos": 1943,
	"sun-wukong": 1944,
	"ah-muzen-cab": 1956,
	"nu-wa": 1958,
	"chaac": 1966,
	"geb": 1978,
	"nemesis": 1980,
	"scylla": 1988,
	"ullr": 1991,
	"kumbhakarna": 1993,
	"janus": 1999,
	"osiris": 2000,
	"rama": 2002,
	"serqet": 2005,
	"cabrakan": 2008,
	"sylvanus": 2030,
	"ao-kuang": 2034,
	"nox": 2036,
	"awilix": 2037,
	"hou-yi": 2040,
	"bellona": 2047,
	"medusa": 2051,
	"ah-puch": 2056,
	"ratatoskr": 2063,
	"ravana": 2065,
	"khepri": 2066,
	"xing-tian": 2072,
	"sol": 2074,
	"chiron": 2075,
	"skadi": 2107,
	"amaterasu": 2110,
	"raijin": 2113,
	"jing-wei": 2122,
	"susano": 2123,
	"fafnir": 2136,
	"erlang-shen": 2138,
	"terra": 2147,
	"izanami": 2179,
	"camazotz": 2189,
	"thoth": 2203,
	"nike": 2214,
	"the-morrigan": 2226,
	"kuzenbo": 2260,
	"cernunnos": 2268,
	"ganesha": 2269,
	"da-ji": 2270,
	"cu-chulainn": 2319,
	"artio": 3336,
	"hachiman": 3344,
	"discordia": 3377,
	"cerberus": 3419,
	"achilles": 3492,
	"chernobog": 3509,
	"baron-samedi": 3518,
	"pele": 3543,
	"hera": 3558,
	"king-arthur": 3565,
	"merlin": 3566,
	"jormungandr": 3585,
	"horus": 3611,
	"set": 3612,
	"olorun": 3664,
	"persephone": 3705,
	"yemoja": 3811,
	"heimdallr": 3812,
	"mulan": 3881,
	"baba-yaga": 3925,
	"cthulhu": 3945,
	"tsukuyomi": 3954,
	"danzaburou": 3984,
	"tiamat": 3990,
	"gilgamesh": 3997,
	"morgan-le-fay": 4006,
	"charybdis": 4010,
	"cliodhna": 4017,
	"atlas": 4034,
	"shiva": 4039,
};

function loadWinrates() {
	var element = document.body;
	var patch = urlParams.get('patch') || currentPatch;

	var god = urlParams.get('god');
	var godID = parseGodName(god);

	if(godID == null) {
		element.innerHTML = 'null';
		return;
	}
	
	var url = 'https://smitelogs.bitbucket.io/gods/winrates/' + patch + '/data.json';
	fetch(url)
		.then(function (response) {
			return response.json();
		})
		.then(function (data) {
			element.innerHTML = getWinratesByGod(data, godID);
		})
		.catch(function (err) {
			console.log(err);
		});
}

function parseGodName(name) {
	return godIDsByName[name.replace(/[^a-z]/gi, '').toLowerCase()];
}

function parseModeName(mode) {
	return gameModeIDsByName[mode.replace(/[^a-z]/gi, '').toLowerCase()];
}

function getKeyByValue(object, value) {
	return Object.keys(object).find(key => object[key] === value);
}

function getWinratesByGod(data, godID) {
	
	var modes = data.filter(g => g.GodId == godID).map(g => g.WinRatesByQueue)[0];
	var dictionary = {};
	
	for(var [key, value] of Object.entries(modes)) {
		var mode = getKeyByValue(gameModeIDsByName, key);
		dictionary[mode] = value + "%";
	}

	return JSON.stringify(dictionary);
}

function loadBuilds() {
	var element = document.body;
	var patch = urlParams.get('patch') || currentPatch;

	var god = urlParams.get('god');
	var godID = parseGodName(god);
	if(godID == null) {
		element.innerHTML = 'null';
		return;
	}

	var mode = urlParams.get('mode') || null;
	var modeID = mode != null ? parseModeName(mode) : null;

	var url = 'https://smitelogs.bitbucket.io/builds/optimal/' + patch + '/' + god + '/data.json';
	fetch(url)
		.then(function (response) {
			return response.json();
		})
		.then(function (data) {
			element.innerHTML = getBuildByGod(data, godID, modeID);
		})
		.catch(function (err) {
			console.log(err);
		});
}

function createBuild(build) {
	var dictionary = {};
	for(var [ndx, item] of Object.entries(build)) {
		addBuildItem(dictionary, ndx, item);
	}
	return dictionary;
}

function addBuildItem(dictionary, key, item) {
	dictionary[key] = {
		Name: item.Name,
		IconUrl: null,
		ItemID: item.ItemId,
		PickRate: null,
		QueueID: item.QueueId,
		WinRate: item.WinRate.toFixed(2) + "%"
	};
}

function getBuildByGod(data, godID, modeID) {
	var dictionary = {};

	if(modeID != null) {
		var build = data.BuildsByQueue[modeID];
		var mode = getKeyByValue(gameModeIDsByName, modeID);
		dictionary = createBuild(build);
	} else {
		var builds = data.BuildsByQueue;
		for(var [mid, build] of Object.entries(builds)) {
			var mode = getKeyByValue(gameModeIDsByName, mid);
			var subDictionary = createBuild(build);
			dictionary[mode] = subDictionary;
		}
	}

	return JSON.stringify(dictionary);
}